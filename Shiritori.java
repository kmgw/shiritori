import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.*;
import java.io.*;


/**
 * 実行クラス
 */
class Shiritori
{

  /**
  * メインメソッド
  * @param String args[] コマンドライン引数
  */
  public static void main(String args[])
  {

    String lastWord   = "しりとり";
    Word firstWord    = new Word(lastWord);
    WordList list     = new WordList(firstWord.getWord());
    boolean repeatFlg = firstWord.firstValidate();
 
    try {

      while (repeatFlg) {

        System.out.println("「" + lastWord + "」に続く言葉を入力してください。");
        
        // 文字入力
        Word newWord = new Word();

        // 入力文字の異常判定
        int result = newWord.validate(lastWord);

        if (result == 0) {
          continue;
        } else if (result == -1) {
          break;
        }

        // 過去の回答との重複チェック
        boolean unique = list.isUnique(newWord);
        if (!unique) {
          System.out.println("過去に出た答えです。");
          break;
        }

        // ここまで通過した場合は入力内容をリストに加える
        list.addWord(newWord);
        // 最終入力内容を更新
        lastWord = list.getLastWord();
      }

    } catch(Exception e) {
      System.out.println("例外" + e + "が発生しました。");
    }
    System.out.println("ゲーム終了です。" + WordList.getCount() + "回続きました。");
  }
}


/**
 * 入力履歴クラス
 * 正しく入力された回答文字列を保持する
 */
class WordList
{

  // 入力履歴リスト
  private List<String> record;
  // 入力数
  private static int count = 0;


  /**
  * インスタンスメソッド
  * @param String firstWord ゲーム開始の文字列
  */
  WordList(String firstWord)
  {
    this.record = new ArrayList<String>();
    this.record.add(firstWord);
  }


  /**
  * 入力した文字列を過去に入力していないことをチェック
  * @param Word input 入力したWordクラスのインスタンス
  * @return boolean型 履歴になかったかどうか 履歴になければtrue
  */
  boolean isUnique(Word input)
  {
    // 入力した文字をString型で取得
    String str = input.getWord();
    int index = this.record.indexOf(str);
    // 0以上だったらリストに載っている = 過去に答えたことがある
    return index >= 0 ? false : true;
  }


  /**
  * 入力文字列を回答一覧リストに追加
  * @param Word input 入力したWordクラスのインスタンス
  */
  void addWord(Word input)
  {
    // 入力した文字をString型で取得
    String str = input.getWord();
    this.record.add(str);
    count++;
  }


  /**
  * 最後の入力内容を取得
  * @return String型 最後に登録したリスト内の文字列
  */
  String getLastWord()
  {
    return this.record.get(this.record.size() - 1);
  }


  /**
  * 継続回数を取得
  * @return int型 登録した言葉の数
  */
  static int getCount()
  {
    return count;
  }

}



/**
 * 単語クラス
 * 入力の度に生成する
 */
class Word
{

  // 入力文字
  private String word;

  // 文字チェック正規表現
  private static final String HIRAGANA_CHECK = "^[\\u3040-\\u3096]+$";


  /**
  * インスタンスメソッド
  * 文字を入力してセット
  */
  Word() throws IOException
  {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    String str = new String(in.readLine());
    str = convertUppercase(str);
    this.word = str;
  }
  
  /**
  * インスタンスメソッド
  * 文字を指定してセット
  */
  Word(String str) 
  {
    str = convertUppercase(str);
    this.word = str;
  }


  /**
  * インスタンス文字取得メソッド
  * @return String型 入力した文字列
  */
  String getWord()
  {
    return this.word;
  }


  /**
  * 末尾の小文字を大文字にする
  * @param String ひらがな文字列
  * @return String型 入力した文字の末尾1文字が小さい文字なら大きい文字に置換して返す
  */
  static String convertUppercase(String inputWords)
  { 
    System.out.println("入力文字列：" + inputWords);

    if (inputWords.length() == 0) {
      return inputWords;
    }


    // 最後の1文字Stringにて取得
    String lastStr = inputWords.substring(inputWords.length() - 1);

    // 検索正規表現パターン作成
    String regex = "ぁ|ぃ|ぅ|ぇ|ぉ|っ|ゃ|ゅ|ょ|ゎ|\\u3095|\\u3063";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(lastStr);

    // 末尾に小文字がある
    if (m.find()) {
      // 置換が必要なので末尾の文字をchar型に変換して変換メソッドへ
      lastStr = convertLast(lastStr.charAt(0));
      // 置換した末尾の文字に入れ替える
      inputWords = inputWords.substring(0, inputWords.length() - 1) + lastStr;
    }
    return inputWords;
  }


  /**
  * 小文字を大文字にする
  * @param char lastChar ひらがな1文字
  * @return String型 小さい文字を大きい文字に入れ替える
  */
  static String convertLast(char lastChar)
  {
    char[] convertBefore = {'ぁ', 'ぃ', 'ぅ', 'ぇ', 'ぉ', 'っ', 'ゃ', 'ゅ', 'ょ', 'ゎ', '\u3095', '\u3063'};
    String[] convertAfter  = {"あ", "い", "う", "え", "お", "つ", "や", "ゆ", "よ", "わ", "か", "け"};
    String lastStr = "";
    
    for (int i = 0; i < convertBefore.length; i++) {
      if (lastChar == convertBefore[i]) {
        lastStr = convertAfter[i];
        break;
      }
    }
    return lastStr;
  }


  /**
  * 指定文字に異常がないかチェック
  * @return boolean型 正常 => true  終了 => false
  */
  boolean firstValidate()
  {
    
    // 末尾文字が「ん」でないかどうか
    boolean result = this.isNotLastDefeat();
    if (!result) {
      System.out.println("「ん」で終わりました。");
      return false;
    }

    // 回答がひらがなのみで構成されているかチェック
    result = this.isOnlyHiragana();
    if (!result) {
      System.out.println("ひらがな以外の文字が含まれています。");
      return false;
    }

    return true;
  }


  /**
  * 入力文字に異常がないかチェック
  * @param String lastWord 最後に正しく回答した文字列
  * @return int型 正常 => 1  入力し直し => 0  終了 => -1
  */
  int validate(String lastWord)
  {
  
    // 1文字以上入力されているかどうか
    if (this.word.length() == 0) {
      System.out.println("文字が入力されませんでした。");
      return 0;
    }

    // 末尾文字が「ん」でないかどうか
    if (!this.isNotLastDefeat()) {
      System.out.println("「ん」で終わりました。");
      return -1;
    }

    // 回答がひらがなのみで構成されているかチェック
    if (!this.isOnlyHiragana()) {
      System.out.println("ひらがな以外の文字が含まれています。");
      return -1;
    }

    // 前回の回答末尾と一致するかチェック、一致しないときは入力しなおし
    if (!this.isMatchLast(lastWord)) {
      System.out.println("先頭の文字が前回の回答の末尾と一致しません。");
      return 0;
    }

    return 1;
  }


  /**
  * 末尾文字が「ん」でないかどうか
  * @return boolean型 「ん」でなければtrue 「ん」ならfalse
  */
  boolean isNotLastDefeat()
  {
    // 最後の1文字取得
    String lastStr = this.word.substring(this.word.length() - 1);
    return lastStr.equals("ん") ? false : true;
  }


  /**
  * ひらがなだけで構成されているかチェック
  * @return boolean型 全部ひらがな => true
  */
  boolean isOnlyHiragana()
  {
    return this.word.matches(HIRAGANA_CHECK);
  }
  

  /**
  * 先頭の文字が前回の文字と一致するかどうか
  * @return boolean型 前回の文字と一致する => true
  */
  boolean isMatchLast(String lastWord)
  {
    // 前回入力の最後の1文字取得
    String lastStr = lastWord.substring(lastWord.length() - 1);
    // 今回入力の最初の1文字取得
    String lead = this.word.substring(0, 1);

    return lastStr.equals(lead) ? true : false;
  }

}